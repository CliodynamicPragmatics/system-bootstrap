#!/usr/bin/env bash

set -e

if ! [ -x "$(command -v git)" ]; then
  sudo apt install git
  git clone https://gitlab.com/CliodynamicPragmatics/system-bootstrap.git ~/.dotfiles
fi

if ! [ -x "$(command -v ansible)" ]; then
  sudo apt install ansible
  sudo apt install python3-psutil
fi

cd ~/.dotfiles/

for file in ./docs/*.org; do
  ./bin/org-babel-tangle.py "${file}"
done

ansible-playbook -K bootstrap.yml
